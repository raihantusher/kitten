<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/admin','AdminController@index')->middleware("auth");
/*
Route::get('/admin/users','UserController@index')->middleware("auth");
Route::get('/admin/users/add','UserController@create')->name("users.create")->middleware("auth");
Route::post('/admin/users/store','UserController@store')->name("users.store")->middleware("auth");

Route::get('/admin/roles','RoleController@index')->middleware("auth");
*/

Route::resource('users','UserController');
