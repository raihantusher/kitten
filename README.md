### Laravel + Core UI template
  * create, edit and delete users
  * included with registration and login page

## Install all the dependencies
```
  >> composer Install
```

## Configure Database into .env file.
## Start migration into Database
```
  >>php artisan migrate:fresh --seed
```
## Then start using
````
>> php artisan serve
```
## Login Details
```
email: admin@admin.com
```
```
password: admin1234
```
